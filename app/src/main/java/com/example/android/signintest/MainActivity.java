package com.example.android.signintest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView continueView = findViewById(R.id.cont);
        // Set a click listener on that View
        continueView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] userData = getUserData();
                // create intent
                Intent profileIntent = new Intent(MainActivity.this, Profile.class);
                // set data
                profileIntent.putExtra("Data",userData);
                // start intent
                startActivity(profileIntent);
            }
        });
    }

    private String[] getUserData() {
        String[] data = new String[4];
        // get data from sign in form
        EditText email = findViewById(R.id.email);
        EditText password = findViewById(R.id.password);
        EditText passwordConfirm = findViewById(R.id.passwordConfirm);
        EditText phoneNumber = findViewById(R.id.mob);
        // set data
        data[0] = email.getText().toString();
        data[1] = password.getText().toString();
        data[2] = passwordConfirm.getText().toString();
        data[3] = phoneNumber.getText().toString();
        return data;
    }
}
