package com.example.android.signintest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Profile extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // set intent
        Intent mainActivity = getIntent();
        // get intent data
        String[] data = mainActivity.getStringArrayExtra("Data");
        // get view by id's
        TextView email = findViewById(R.id.email_view);
        TextView password = findViewById(R.id.password_view);
        TextView passwordConfirm = findViewById(R.id.password_confirm_view);
        TextView phoneNumber = findViewById(R.id.phone_number_view);
        // set text view values
        email.setText(data[0]);
        password.setText(data[1]);
        passwordConfirm.setText(data[2]);
        phoneNumber.setText(data[3]);
    }
}
